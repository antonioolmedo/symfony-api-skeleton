## Setup environment & Install & Build application
setup: create-network up composer

## Tear down environment
teardown: down remove-network

## Creates docker network
create-network:
	docker network create nginx_network

## Removes docker network
remove-network:
	docker network rm nginx_network

## Execute composer install inside first php container
composer:
	docker exec -w /app symfony_api_skeleton_php_1 composer install

## Start docker
up:
	docker-compose up -d

## Stop docker
down:
	docker-compose down


## Go to container terminal
exec:
	docker exec -ti ${container} /bin/bash

## Go to PHP container terminal
php-terminal:
	 @$(MAKE) exec container=symfony_api_skeleton_php_1

## Go to Database container terminal
db-terminal:
	 @$(MAKE) exec container=symfony_api_skeleton_db_1
