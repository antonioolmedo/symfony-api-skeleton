# Symfony API Skeleton
## Introduction
This repository is created to provide a Symfony API application skeleton. You may need to modify files to match your project.

## Installation
First copy `env.dist` file and configure with your custom params and then execute `make` to build and up all dockers container and composer install.

## Other make commands
* `make teardown`: Will call `down` and `remove-network` targets.
* `make create-network`: Will creates required docker network.
* `make remove-network`: Will removes required docker network.
* `make composer`: Will connect to php container and run `composer install`.
* `make up`: Will starts docker containers.
* `make down`: Will stop and remove docker containers.
* `make exec container=CONTAINER_NAME`: Will connect to named docker container's terminal.
* `make php-terminal`: Will uses `exec` to connect to PHP docker container terminal.
* `make db-terminal`: Will uses `exec` to connect to PHP docker container terminal.

## XDebug
Only you need is configure XDebug port (`.env`) on your IDE. The IDE key is `docker` if you need to configure it.

## Nginx vhost configuration
All files included in the folder `nginx.conf.d` will be automatically included as part of default nginx vhost.